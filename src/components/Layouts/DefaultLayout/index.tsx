import React from 'react'
import Footer from '~/components/Footer'
import Header from '~/components/Header'

type DefaultLayoutPropsType = {
  children: React.ReactNode
}

const DefaultLayout: React.FC<DefaultLayoutPropsType> = ({ children }) => {
  return (
    <div className='default-layout'>
      <Header />
      <div className='container'>{children}</div>
      <Footer />
    </div>
  )
}

export default DefaultLayout
