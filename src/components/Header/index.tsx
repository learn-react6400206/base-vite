import React from 'react'
import { Link } from 'react-router-dom'
import classNames from 'classnames/bind'
import styles from './Header.module.scss'

const cx = classNames.bind(styles)

const Header: React.FC = () => {
  return (
    <header className={cx('wrapper-header')}>
      <Link to='/'>
        <img src='/vite.svg' alt='logo' />
      </Link>
      <p>Header</p>
    </header>
  )
}

export default Header
