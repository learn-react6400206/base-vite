import React, { ReactElement } from 'react'
import classNames from 'classnames/bind'
import styles from './Button.module.scss'
import { Link } from 'react-router-dom'

const cx = classNames.bind(styles)

type ButtonPropsType = {
  children: React.ReactNode
  to?: string | React.ReactNode | ReactElement
  href?: string | React.ReactNode | ReactElement
  onClick?: () => void | React.ReactNode
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  passProps?: React.ReactNode | React.ReactElement<any, string | React.JSXElementConstructor<any>> | any
  primary?: boolean
  secondary?: boolean
  success?: boolean
  danger?: boolean
  warning?: boolean
  info?: boolean
  light?: boolean
  dark?: boolean
  outline?: boolean
  disabled?: boolean
  rounded?: boolean
  className: string
}

const Button: React.FC<ButtonPropsType> = ({
  children,
  to,
  href,
  onClick,
  passProps,
  primary = false,
  secondary = false,
  success = false,
  danger = false,
  warning = false,
  info = false,
  light = false,
  dark = false,
  outline = false,
  disabled = false,
  rounded = false,
  className
}) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let TagWrap = 'button' as any

  const props = {
    onClick,
    ...passProps
  }

  // Remove event listener when button is disabled
  if (disabled) {
    Object.keys(props).forEach((key) => {
      if (key.startsWith('on') && typeof props[key] === 'function') {
        delete props[key]
      }
    })
  }

  if (to) {
    props.to = to
    TagWrap = Link
  } else if (href) {
    props.href = href
    TagWrap = 'a'
  }

  const classes = cx('default-button', 'button-wrapper', {
    primary,
    secondary,
    success,
    danger,
    warning,
    info,
    light,
    dark,
    to,
    outline,
    rounded,
    disabled,
    [className]: className
  })

  return (
    <TagWrap className={classes} {...props}>
      <span className={cx('button-text')}>{children}</span>
    </TagWrap>
  )
}

export default Button
