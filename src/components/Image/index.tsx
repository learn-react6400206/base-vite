import { default as React, forwardRef, useState } from 'react'
import classNames from 'classnames'
import images from '~/assets/images'
import styles from './Image.module.scss'

export type ImageProps = {
  src: string
  alt: string
  className: string
  customFallback: string
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const Image = forwardRef<any, ImageProps>(
  ({ src, alt, className, customFallback = images.noImage, ...props }, ref) => {
    const [fallback, setFallback] = useState('')

    const handleError = () => {
      setFallback(customFallback)
    }

    return (
      <img
        src={fallback || src}
        {...props}
        alt={alt}
        onError={handleError}
        className={classNames(styles.wrapper, className)}
        ref={ref}
      />
    )
  }
)

export default Image
