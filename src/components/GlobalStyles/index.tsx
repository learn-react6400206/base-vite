import React from 'react'
import './GlobalStyles.scss'

type GlobalStylesPropsType = {
  children: React.ReactNode
}

const GlobalStyles: React.FC<GlobalStylesPropsType> = ({ children }) => {
  return <>{children}</>
}

export default GlobalStyles
