import React from 'react'
import Button from '~/components/Button'
const LIST_BUTTONS = ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark', 'link']
const Home: React.FC = () => {
  return (
    <>
      <h1>Home page</h1>
      {LIST_BUTTONS.map((button, index) => (
        <Button key={index} className={button} to={button === 'link' ? 'login' : ''}>
          {button}
        </Button>
      ))}
    </>
  )
}

export default Home
