/* eslint-disable @typescript-eslint/no-explicit-any */
import { Fragment } from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

import { routes } from '~/routes'
import DefaultLayout from '~/components/Layouts/DefaultLayout'

function App() {
  return (
    <Router>
      <div className='App'>
        <Routes>
          {routes.map((route: any, index) => {
            let Layout = DefaultLayout
            if (route.layout) {
              Layout = route.layout
            } else if (route.layout === null) {
              Layout = Fragment
            }
            const Page = route.component
            return (
              <Route
                key={index}
                path={route.path}
                element={
                  <Layout>
                    <Page />
                  </Layout>
                }
              />
            )
          })}
        </Routes>
      </div>
    </Router>
  )
}

export default App
