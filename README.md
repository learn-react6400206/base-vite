# Base vite project

### Dev commands

- npm install
- npm run dev

### Build commands

- npm run build

### Others

- Preview kết quả build bằng câu lệnh npm run preview
- Kiểm tra dự án có bị lỗi gì liên quan ESLint không: npm run lint
- Tự động fix các lỗi liên quan ESLint (không phải cái gì cũng fix được, nhưng fix cũng nhiều): npm run lint:fix
- Kiểm tra dự án có bị lỗi gì liên quan Prettier không: npm run prettier
- Tự động fix các lỗi liên quan Prettier: npm run prettier:fix

### Config ESlint

- File .eslintignore để giúp eslint bỏ qua những file không cần thiết

### Config Prettier format code

- File .prettierrc để cấu hình prettier
- Để VS Code hiểu được file này thì cài Extension Prettier - Code formatter for VS Code
- File .prettierignore để Prettier bỏ qua các file không cần thiết

### Config editor để chuẩn hóa cấu hình editor

- File .editorconfig cấu hình các config đồng bộ các editor với nhau nếu dự án có nhiều người tham gia
- Để VS Code hiểu được file này thì cài Extension là EditorConfig for VS Code

### Config alias cho tsconfig.json

- Thêm alias vào compilerOptions trong file tsconfig.json giúp VS Code hiểu mà tự động import:
  "baseUrl": ".",
  "paths": {
  "~/_": ["src/_"]
  }

### Config alias cho vite vite.config.ts

- Cài package @types/node để sử dụng node js trong file ts không bị lỗi: npm i @types/node -D
- Cấu hình alias và enable source map ở file vite.config.ts

### Cập nhật script cho package.json

- Mở file package.json lên, thêm đoạn script dưới vào:
  "scripts": {
  //...
  "lint": "eslint --ext ts,tsx src/",
  "lint:fix": "eslint --fix --ext ts,tsx src/",
  "prettier": "prettier --check \"src/**/(_.tsx|_.ts|_.css|_.scss)\"",
  "prettier:fix": "prettier --write \"src/**/(_.tsx|_.ts|_.css|_.scss)\""
  }

### Style

- Sử dụng thư viện classnames để hỗ trợ khai báo class trong jsx ngắn gọn
- Đặt tên class dạng class-name
- Reset, default, style global css trong file GlobalStyles.scss
